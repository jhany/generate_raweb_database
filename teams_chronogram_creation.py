import logging, configparser
from pymongo import MongoClient
# Config properties
config = configparser.RawConfigParser()
config.read("ConfigFile.properties")

# Logger configuration
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__file__.split('/')[-1])

mongodb_ip = config.get("mongodb", "ip")
mongodb_port = int(config.get("mongodb", "port"))
mongodb_db = config.get("mongodb", "database")

# MongoDB client
client = MongoClient(mongodb_ip, mongodb_port)


def data_to_mongodb(json):
    client = MongoClient(mongodb_ip, mongodb_port)
    db = client.get_database(mongodb_db)
    try:
        db.teams_chronogram.insert(json)
        client.close()
    except BaseException as e:
        logger.warning(str(e))
        pass


if __name__ == '__main__':
    db = client.get_database(mongodb_db)
    db.teams_chronogram.drop()
    teams = db.teams.distinct("acronym")
    teams.sort()
    for team in teams:
        team_with_chronogram = dict()
        teams_with_duplicates = db.teams.find({"acronym": team})
        team_with_chronogram["acronym"] = team
        list_chronogram = []
        for duplicate_team in teams_with_duplicates:
            chronogram = dict()
            chronogram["year"] = duplicate_team["year"]
            chronogram["members"] = duplicate_team["members"]
            list_chronogram.append(chronogram)
        #list_chronogram.sort()
        team_with_chronogram["chronogram"] = list_chronogram
        data_to_mongodb(team_with_chronogram)
        #sorted(list_chronogram, key=itemgetter('year'))
        #print(team_with_chronogram)
